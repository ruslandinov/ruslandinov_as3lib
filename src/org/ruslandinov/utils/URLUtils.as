package org.ruslandinov.utils
{
import flash.external.ExternalInterface;
import flash.net.URLRequest;
import flash.net.URLVariables;
import flash.net.navigateToURL;

import org.ruslandinov.logger.Logger;

/**
 * ...
 * @author Ruslan Gilmutdinov
 */
public class URLUtils
{

    public static function openURL(url:String, target:String = '_blank'):void
    {
        if (CapabilitiesGetter.isBrowserIE)
        {
            try
            {
                Logger.log('URLUtils', 'openURL(): trying window.open url=', url, ' target=', target);

                ExternalInterface.call('window.open', url, target);
            }
            catch (error:Error)
            {
                if (error.toString() != 'InternalError: too much recursion')
                {
                    Logger.log('URLUtils', 'openURL(): error, using navigateToURL url=', url, ' target=', target);

                    navigateToURL(new URLRequest(url), target);
                }
            }
        }
        else
        {
            Logger.log('URLUtils', 'openURL(): using navigateToURL url=', url, ' target=', target);

            navigateToURL(new URLRequest(url), target);
        }
    }

    public static function addGetParamsToURL(url:String, variables:URLVariables):String
    {
        var delimeter:String = url.indexOf('?') > -1 ? '&' : '?';
        return url + delimeter + variables.toString();
    }

    public static function checkExtension(url:String, extension:String):Boolean
    {
        var regexp:RegExp = new RegExp("\\." + extension + "$|\\." + extension + "\\?{1}.+", "i");

        return url && regexp.test(url);
    }

}
}