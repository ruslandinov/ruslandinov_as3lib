package org.ruslandinov.utils
{
import flash.system.Capabilities;

import org.ruslandinov.js.ExternalInterfaceWrapper;

/**
* ...
* @author Ruslan Gilmutdinov
*/

public class CapabilitiesGetter
{

    private static var _capabilities:String;
    private static var _browserVersion:String;

    private static function getCapabilities():String
    {
        _capabilities = '\n'+
            'avHardwareDisable      = ' + Capabilities.avHardwareDisable       + '\n' +
            'hasAccessibility       = ' + Capabilities.hasAccessibility        + '\n' +
            'hasAudio               = ' + Capabilities.hasAudio                + '\n' +
            'hasAudioEncoder        = ' + Capabilities.hasAudioEncoder         + '\n' +
            'hasEmbeddedVideo       = ' + Capabilities.hasEmbeddedVideo        + '\n' +
            'hasIME                 = ' + Capabilities.hasIME                  + '\n' +
            'hasMP3                 = ' + Capabilities.hasMP3                  + '\n' +
            'hasPrinting            = ' + Capabilities.hasPrinting             + '\n' +
            'hasScreenBroadcast     = ' + Capabilities.hasScreenBroadcast      + '\n' +
            'hasScreenPlayback      = ' + Capabilities.hasScreenPlayback       + '\n' +
            'hasStreamingAudio      = ' + Capabilities.hasStreamingAudio       + '\n' +
            'hasStreamingVideo      = ' + Capabilities.hasStreamingVideo       + '\n' +
            'hasTLS                 = ' + Capabilities.hasTLS                  + '\n' +
            'hasVideoEncoder        = ' + Capabilities.hasVideoEncoder         + '\n' +
            'isDebugger             = ' + Capabilities.isDebugger              + '\n' +
            'isEmbeddedInAcrobat    = ' + Capabilities.isEmbeddedInAcrobat     + '\n' +
            'language               = ' + Capabilities.language                + '\n' +
            'localFileReadDisable   = ' + Capabilities.localFileReadDisable    + '\n' +
            'manufacturer           = ' + Capabilities.manufacturer            + '\n' +
            'os                     = ' + Capabilities.os                      + '\n' +
            'pixelAspectRatio       = ' + Capabilities.pixelAspectRatio        + '\n' +
            'playerType             = ' + Capabilities.playerType              + '\n' +
            'screenColor            = ' + Capabilities.screenColor             + '\n' +
            'screenDPI              = ' + Capabilities.screenDPI               + '\n' +
            'screenResolutionX      = ' + Capabilities.screenResolutionX       + '\n' +
            'screenResolutionY      = ' + Capabilities.screenResolutionY       + '\n' +
            'version                = ' + Capabilities.version;

        return _capabilities;
    }

    public static function get capabilities():String
    {
        return _capabilities ? _capabilities : getCapabilities();
    }

    private static function getBrowserVersion():String
    {
        _browserVersion = ExternalInterfaceWrapper.call('window.navigator.userAgent.toString');

        return _browserVersion;
    }

    public static function get browserVersion():String
    {
        return _browserVersion ? _browserVersion : getBrowserVersion();
    }

    public static function get isBrowserIE():Boolean
    {
        return browserVersion && browserVersion.indexOf('MSIE') > -1;
    }

    public static function get isPepperFlash():Boolean
    {
        return Capabilities.manufacturer == "Google Pepper";
    }
}
}