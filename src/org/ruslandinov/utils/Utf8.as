package org.ruslandinov.utils
{
public class Utf8
{
    // public method for url encoding
    public static function encode(string:String):String
    {

        var utftext:String = "";
        var c:Number;

        for (var n:Number = 0; n < string.length; n++) {


            c = string[n];

            if (c < 128) {
                utftext += c;
            }
            else if((c > 127) && (c < 2048)) {
                utftext += ((c >> 6) | 192);
                utftext += ((c & 63) | 128);
            }
            else {
                utftext += ((c >> 12) | 224);
                utftext += (((c >> 6) & 63) | 128);
                utftext += ((c & 63) | 128);
            }

        }

        return utftext;
    }

    // public method for url decoding
    public static function decode(utftext:String):String
    {
        var string:String = "";
        var i:Number = 0;
        var c:Number = 0;
        var c1:Number = 0;
        var c2:Number = 0;
        var c3:Number = 0;

        while ( i < utftext.length )
        {

            c = utftext.charCodeAt(i);

            if (c < 128)
            {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224))
            {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode((((c & 31) << 6) | (c2 & 63)));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode((((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)));
                i += 3;
            }

        }

        return string;
    }
}
}