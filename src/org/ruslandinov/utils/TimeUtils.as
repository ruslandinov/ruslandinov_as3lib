package org.ruslandinov.utils
{
/**
 * @author ruslandinov
 */
public class TimeUtils
{
    public static function timeStringToSeconds(string:String):Number
    {
        if (!string || !string.length)
        {
            return NaN;
        }

        var parts:Array = string.split(':');
        var multipliers:Array = [3600, 60, 1];
        var i:int = 0;
        var seconds:Number = 0;
        while (parts.length && i <= 3)
        {
            seconds += parts.pop() * multipliers.pop();
            i++;
        }

        return i > 0 ? seconds : NaN;
    }

    public static function seconds2String(seconds:Number):String
    {
        var intHours:int   = seconds / 3600;
        var intMinutes:int = int(seconds % 3600) / 60;
        var intSeconds:int = int(seconds % 3600) % 60;

        var timeParts:Array = intHours ? [intHours, intMinutes, intSeconds] : [intMinutes, intSeconds];
        var timePartsToString:Array = timeParts.map(
                function (item:int, ...rest):String {
                    return item < 10 ? '0' + String(item) : String(item);
                }
        );

        return timePartsToString.join(':');
    }

}
}
