package org.ruslandinov.utils
{
    import flash.xml.XMLDocument;

/**
 * ...
 * @author Ruslan Gilmutdinov
 */
public class StringUtils
{

    public static function htmlEntityDecode(input:String):String
    {
        var xml:XMLDocument = new XMLDocument(input);

        return xml && xml.firstChild ? xml.firstChild.nodeValue : null;
    }

    public static function hexColorToUint(hexString:String):uint
    {
        return parseInt(hexString.replace('#', ''), 16);
    }

    public static function parseColor(value:*):uint
    {
        if (value is Number)
        {
            return value;
        }
        else if (value is String)
        {
            var match:Array;
            if (/^#.+/.test(value))
            {
                match = /^#(.+)/.exec(value);
                return match && match.length > 1 ? parseInt(match[1],  16) : 0;
            }
            else if (/^0x(.+)/.test(value))
            {
                match = /^0x(.+)/.exec(value);
                return match && match.length > 1 ? parseInt(match[1],  16) : 0;
            }
            else
            {
                return parseInt(value, 16);
            }
        }
        else
        {
            return 0x000000;
        }
    }

    public static function secondsRusSuffix(seconds:uint):String
    {
        var result:String;

        if (seconds == 0)
        {
            result = 'секунд';
        }
        else if (seconds == 1)
        {
            result = 'секунда';
        }
        else if (seconds < 5)
        {
            result = 'секунды';
        }
        else if (seconds < 21)
        {
            result = 'секунд';
        }
        else
        {
            var lastDigit:uint = seconds % 10;

            result = secondsRusSuffix(lastDigit);
        }

        return result;
    }

}
}