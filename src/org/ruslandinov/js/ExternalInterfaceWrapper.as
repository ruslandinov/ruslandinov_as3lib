package org.ruslandinov.js
{
    import flash.external.ExternalInterface;
    import org.ruslandinov.logger.Logger;

    /**
     * ...
     * @author Ruslan Gilmutdinov
     */
    public class ExternalInterfaceWrapper
    {

        public static function get marshallExceptions():Boolean
        {
            return ExternalInterface.marshallExceptions;
        }

        public static function set marshallExceptions(value:Boolean):void
        {
            ExternalInterface.marshallExceptions = value;
        }

        public static function get available():Boolean
        {
            return ExternalInterface.available;
        }

        public static function get objectID():String
        {
            return ExternalInterface.objectID;
        }

        public static function addCallback(functionName:String, closure:Function):void
        {
            try
            {
                ExternalInterface.addCallback(functionName, closure);
            }
            catch (error:Error)
            {
                Logger.log("[ExternalInterfaceWrapper]", "addCallback(): error=", error);
            }
        }

        public static function call(functionName:String, ...args):*
        {
            var result:*;

            try
            {
                var params:Array = args;
                params.unshift(functionName);
                result = ExternalInterface.call.apply("[ExternalInterfaceWrapper]", params);
            }
            catch (error:Error)
            {
                Logger.log("[ExternalInterfaceWrapper]", "call(): error executing functionName=", functionName, " args=", args, "error=", error);
            }

            return result;
        }

    }
}