﻿package org.ruslandinov.gif
{
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.display.PixelSnapping;
import flash.events.ErrorEvent;
import flash.events.Event;
import flash.events.TimerEvent;
import flash.geom.Rectangle;
import flash.utils.ByteArray;
import flash.utils.Timer;

public class GIFAnimator extends Bitmap
{
    //Время отображения кадра по умолчанию в десятках миллисукунд
    private static const DEFAULT_DELAY:uint = 5;

    //Класс, считавыющий и хранящий параметры анимации и изображений
    private var reader:GIFReader;

    //Массивы загрузчиков и изображений
    private var loaders:Array = [];
    private var images:Array = [];
    private var imageCounter:uint = 0;

    private var imgBitmapData:BitmapData;

    private var frameTimer:Timer;
    private var currFrame:uint = 0;

    public function GIFAnimator()
    {
        pixelSnapping = PixelSnapping.AUTO;
        smoothing = true;
    }

    public function set bytes(value:ByteArray):void
    {
        reset();

        reader = new GIFReader(value);
        loadImages();
    }

    public function free():void
    {
        reset();
    }

    private function reset():void
    {
        freeTimer();
        currFrame = 0;
        imageCounter = 0;

        loaders = [];
        images = [];

        freeGIFReader();
        freeBitmapData();
    }

    private function freeGIFReader():void
    {
        if (reader)
        {
            reader.free();
            reader = null;
        }
    }

    private function freeBitmapData():void
    {
        if (imgBitmapData)
        {
            imgBitmapData.dispose();
            imgBitmapData = null;

            bitmapData = null;
        }
    }

    //Создание массива объектов Loader
    //для преобразования байтовых массивов в BitmapData
    private function loadImages():void
    {
        for (var i:int = 0; i < reader.frameNum; i++)
        {
            loaders[i] = new Loader();
            loaders[i].contentLoaderInfo.addEventListener(Event.COMPLETE, completeListener);
            loaders[i].loadBytes(reader.getFrameBytes(i));
        }
    }

    //Успешная загрузка изображения
    private function completeListener(event:Event):void
    {
        try
        {
            images[loadedImageNum(event)] = Bitmap(event.target.content).bitmapData;
        }
        catch(error:Error)
        {
        }

        imageCounter++;
        if (imageCounter == reader.frameNum)
        {
            deleteLoaders();
            createImage();
        }
    }

    //Определение номера загруженнай картинки
    private function loadedImageNum(event:Event):uint
    {
        for (var i:int = 0; i < reader.frameNum; i++)
        {
            if (event.target === loaders[i].contentLoaderInfo)
            {
                return i;
            }
        }
        return 0;
    }

    //Удаление всех загрузчиков
    private function deleteLoaders():void
    {
        for (var i:int = 0; i < reader.frameNum; i++)
        {
            loaders[i].contentLoaderInfo.removeEventListener(Event.COMPLETE, completeListener);
        }
        loaders = [];
    }

    //Создание изображения
    private function createImage():void
    {
        if (images.length)
        {
            imgBitmapData = new BitmapData(reader.width, reader.height, true, 0xFF);
            this.bitmapData = imgBitmapData;

            drawImage();

            if (images.length > 1)
            {
                createTimer();
                startTimer();
            }

            dispatchEvent(new Event(Event.COMPLETE));
        }
        else
        {
            dispatchEvent(new ErrorEvent(ErrorEvent.ERROR));
        }
    }

    //Создание таймеров
    private function createTimer():void
    {
        frameTimer = new Timer(0, 1);
        frameTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onFrameTimerComplete);
    }

    private function freeTimer():void
    {
        if (frameTimer)
        {
            frameTimer.removeEventListener(TimerEvent.TIMER, onFrameTimerComplete);
            frameTimer.reset();
            frameTimer = null;
        }
    }

    //Запуск таймеров
    private function startTimer():void
    {
        setTimerDelay();
        frameTimer.repeatCount = 1;

        frameTimer.start();
    }

    //Сборос таймеров
    private function restartTimer():void
    {
        frameTimer.reset();

        startTimer();
    }

    private function setTimerDelay():void
    {
        frameTimer.delay = (reader.getFrameDelays(currFrame) || DEFAULT_DELAY) * 10;
    }

    //Срабатывание таймера
    private function onFrameTimerComplete(event:TimerEvent):void
    {
        currFrame++;
        if (currFrame == images.length)
        {
            currFrame = 0;
        }

        drawImage();

        restartTimer();
    }

    //Рисование изображения
    private function drawImage():void
    {
        imgBitmapData.lock();

        //Первый кадр
        if (currFrame == 0)
        {
            clearImage();
        }

        var frameDisplacement:uint = reader.getFrameDisplacement(currFrame);
        //Замещение изображения
        if (frameDisplacement == 3)
        {
            clearImage();
        }

        drawCurrImage();

        imgBitmapData.unlock();
    }

    //Очистка изображения
    private function clearImage():void
    {
        var rect:Rectangle = new Rectangle(0, 0, reader.width, reader.height);
        imgBitmapData.fillRect(rect, 0xFF);
    }

    //Рисование текушего кадра поверх изображения
    private function drawCurrImage():void
    {
        var b:BitmapData = new BitmapData(reader.width, reader.height, true, 0xFF);
        b.copyPixels(images[currFrame], reader.getFrameRect(currFrame), reader.getFramePoint(currFrame));
        imgBitmapData.draw(b);
    }

    //Ширина изображения
    public function get originalWidth():Number
    {
        return reader.width;
    }

    //Высота изображения
    public function get originalHeight():Number
    {
        return reader.height;
    }
}
}