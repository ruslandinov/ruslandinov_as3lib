﻿package org.ruslandinov.gif
{
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.ByteArray;

public class GIFReader
{
    //Байтовые массивы
    private var description:ByteArray;
    private var expand:Array = [];
    private var image:Array = [];

    //Дескриптор логического экрана
    private var _version:String;
    private var _width:uint;
    private var _height:uint;
    private var _globalColorSize:uint;

    //Массивы со свойствами изоражения
    private var framesRect:Array = [];
    private var framesPoint:Array = [];
    private var frameBytes:Array = [];
    private var framesDisplacement:Array = [];
    private var framesDelay:Array = [];
    private var colorSize:Array = [];

    //Исходный массив
    private var byteArray:ByteArray;


    public function GIFReader(_byteArray:ByteArray)
    {
        byteArray = _byteArray;

        init();
    }

    public function free():void
    {
        byteArray = null;
    }

    //Инициализация
    private function init():void
    {
        if (checkVersion())
        {
            readDescription();
            readData();
        }
    }

    //Проверка версии файла
    private function checkVersion():Boolean
    {
        _version = byteArray.readUTFBytes(6);

        return _version == "GIF89a" || _version == "GIF87a";
    }

    //Считывание параметров из дескриптора
    private function readDescription():void
    {
        _width = readDoubleByte();
        _height = readDoubleByte();
        _globalColorSize = readColorSize();
        byteArray.position += 2 + _globalColorSize * 3;

        description = new ByteArray();
        description.writeBytes(byteArray, 0, byteArray.position);
    }

    //Считывание изображение и расширений
    private function readData():void
    {
        while (readGraphicExpansion() || readApplicationExpansion() || readImage() || readCommentExpansion() || readTextExpansion())
        {

        }

        createArray();
    }

    //Чтение расширения графики
    private function readGraphicExpansion():Boolean
    {
        if (String.fromCharCode(byteArray[byteArray.position]) == "!" && byteArray[byteArray.position + 1] == 0xF9 && byteArray[byteArray.position + 2] == 0x04 && byteArray[byteArray.position + 7] == 0x00)
        {
            var start:uint = byteArray.position;

            byteArray.position += 3;
            readDisp();
            framesDelay[image.length] = readDoubleByte();
            byteArray.position += 2;

            var end:uint = byteArray.position;
            var b:ByteArray = new ByteArray();
            b.writeBytes(byteArray, start, end - start);
            expand[image.length] = b;

            return true;

        }
        return false;
    }

    //Чтение расширения приложения
    private function readApplicationExpansion():Boolean
    {
        if (String.fromCharCode(byteArray[byteArray.position]) == "!" && byteArray[byteArray.position + 1] == 0xFF && byteArray[byteArray.position + 2] == 0x0B)
        {
            byteArray.position += 14;
            readBlocks();

            return true;

        }
        return false;
    }

    //Чтение расширения простого текста
    private function readTextExpansion():Boolean
    {
        if (String.fromCharCode(byteArray[byteArray.position]) == "!" && byteArray[byteArray.position + 1] == 0x01 && byteArray[byteArray.position + 2] == 0x0C)
        {
            byteArray.position += 15;
            readBlocks();

            return true;

        }
        return false;
    }

    //Чтение расширения комментария
    private function readCommentExpansion():Boolean
    {
        if (String.fromCharCode(byteArray[byteArray.position]) == "!" && byteArray[byteArray.position + 1] == 0xFE)
        {
            byteArray.position += 2;
            readBlocks();

            return true;

        }
        return false;
    }

    //Чтение изображения
    private function readImage():Boolean
    {
        if (String.fromCharCode(byteArray[byteArray.position]) == ",")
        {
            var start:uint = byteArray.position;

            byteArray.position += 1;

            var rect:Rectangle = new Rectangle(readDoubleByte(), readDoubleByte(), readDoubleByte(), readDoubleByte());
            var point:Point = new Point(rect.x, rect.y);
            framesRect.push(rect);
            framesPoint.push(point);

            colorSize.push(readColorSize());
            byteArray.position += 1;
            byteArray.position += colorSize[colorSize.length - 1] * 3;
            readBlocks();

            var end:uint = byteArray.position;
            var b:ByteArray = new ByteArray();
            b.writeBytes(byteArray, start, end - start);
            image.push(b);

            return true;
        }
        return false;
    }

    //Считывание способа замены изображения
    private function readDisp():void
    {
        var one_byte:uint = byteArray.readUnsignedByte();
        var array:Array = new Array(8);
        for (var i:int = 7; i >= 0; i--)
        {
            if (one_byte > Math.pow(2, i))
            {
                array[i] = true;
                one_byte -= Math.pow(2, i);
            }
        }

        framesDisplacement[image.length] = uint(array[2]) + 2 * uint(array[3]) + 4 * uint(array[4]);
    }

    //Считавыние целого числа из двух байтов
    private function readDoubleByte():uint
    {
        var value:uint = byteArray.readUnsignedByte();
        value += 256 * byteArray.readUnsignedByte();
        return value;
    }

    //Сичитывание количества цветов
    private function readColorSize():uint
    {
        var one_byte:uint = byteArray.readUnsignedByte();
        var array:Array = new Array(8);
        for (var i:int = 7; i >= 0; i--)
        {
            if (one_byte > Math.pow(2, i))
            {
                array[i] = true;
                one_byte -= Math.pow(2, i);
            }
        }

        if (array[7])
        {
            var size:uint = uint(array[0]) + 2 * uint(array[1]) + 4 * uint(array[2]);
            return Math.pow(2, size + 2);
        }
        else
        {
            return 0;
        }
    }

    //Считывание блоков
    private function readBlocks():void
    {
        while (byteArray[byteArray.position] != 0)
        {
            byteArray.position += byteArray.readUnsignedByte() + 1;
        }
        byteArray.position += 1;
    }

    //Создание байтовых массивов для каждого изображения
    private function createArray():void
    {
        var b:ByteArray;
        for (var i:int = 0; i < image.length; i++)
        {
            b = new ByteArray();
            b.writeBytes(description);
            if (expand[i])
            {
                b.writeBytes(expand[i]);
            }
            b.writeBytes(image[i]);
            frameBytes.push(b);
        }
    }

    public function getFramePoint(index:int):Point
    {
        return framesPoint[index];
    }

    public function getFrameRect(index:int):Rectangle
    {
        return framesRect[index];
    }

    public function getFrameBytes(index:int):ByteArray
    {
        return frameBytes[index];
    }

    public function getFrameDisplacement(index:int):uint
    {
        return framesDisplacement[index];
    }

    public function getFrameDelays(index:int):uint
    {
        return framesDelay[index];
    }

    public function get frameNum():int
    {
        return frameBytes.length;
    }

    public function get width():uint
    {
        return _width;
    }

    public function get height():uint
    {
        return _height;
    }

    public function get version():String
    {
        return _version;
    }

    public function get globalColorSize():uint
    {
        return _globalColorSize;
    }
}
}