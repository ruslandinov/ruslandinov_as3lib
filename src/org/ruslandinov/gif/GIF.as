package org.ruslandinov.gif
{
import flash.display.Sprite;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLRequest;
import flash.net.URLStream;
import flash.utils.ByteArray;

import org.ruslandinov.logger.Logger;

/**
 * @author ruslandinov
 */
public class GIF extends Sprite
{
    private var urlStream:URLStream;
    private var gifAnimator:GIFAnimator;

    public function GIF()
    {
        createLoader();

        mouseChildren = false;
    }

    private function createLoader():void
    {
        urlStream = new URLStream();
        urlStream.addEventListener(Event.COMPLETE, onLoaded);
        urlStream.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
        urlStream.addEventListener(IOErrorEvent.IO_ERROR, onError);
    }

    public function free():void
    {
        freeLoader();
        freeGIFAnimator();
    }

    private function freeLoader():void
    {
        urlStream.removeEventListener(Event.COMPLETE, onLoaded);
        urlStream.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
        urlStream.removeEventListener(IOErrorEvent.IO_ERROR, onError);

        try
        {
            urlStream.close();
        }
        catch (error:Error)
        {

        }
    }

    private function freeGIFAnimator():void
    {
        if (gifAnimator)
        {
            gifAnimator.parent.removeChild(gifAnimator);
            gifAnimator.removeEventListener(Event.COMPLETE, onImageReady);
            gifAnimator.free();
            gifAnimator = null;
        }
    }

    private function onError(event:Event):void
    {
        Logger.log(this, 'onError(): event=', event);

        dispatchEvent(event);
    }

    private function onLoaded(event:Event):void
    {
        createGIFAnimator();
    }

    private function createGIFAnimator():void
    {
        var bytes:ByteArray = new ByteArray();
        urlStream.readBytes(bytes);

        gifAnimator = new GIFAnimator();
        gifAnimator.bytes = bytes;
        gifAnimator.addEventListener(Event.COMPLETE, onImageReady);
        addChild(gifAnimator);
    }

    private function onImageReady(event:Event):void
    {
        dispatchEvent(event);
    }

    public function load(url:String):void
    {
        urlStream.load(new URLRequest(url));
    }
}
}
