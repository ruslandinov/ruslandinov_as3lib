package org.ruslandinov.logger {
import flash.events.IEventDispatcher;

/**
 * @author Ruslan Guilmutdinov
 */
public interface ILogger extends IEventDispatcher
{
    function free():void;

    function log(message:*, from:*= null):String;

    function getLog():String;

    function get size():int;

    function postTo(url:String):void;
}
}
