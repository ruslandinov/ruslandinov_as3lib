package org.ruslandinov.logger {

import flash.events.EventDispatcher;
import flash.net.URLRequest;
import flash.net.URLRequestMethod;
import flash.net.URLVariables;
import flash.net.sendToURL;

/**
 * @author Ruslan Guilmutdinov
 */
public class BasicLogger extends EventDispatcher implements ILogger
{
    private static const DELIMITERS:String          = " :,.!@#$%^&*()_+=[]{}/|?`\~";
    private static const LOG_BEGINING:String        = "------------BEGINING------------";

    private var logStrings:String                   = '';

    public function BasicLogger()
    {
        super();
        log(LOG_BEGINING);
    }

    public function free():void
    {
        logStrings = null;
    }

    protected static function prepareLogString(service:*, message:*):String
    {
        var now:Date = new Date();
        var timePrefix:String = '<' + now.toTimeString().split(' ')[0] + '> ';
        var serviceString:String = service ? service + '::' : '';

        var messageString:String = '';
        if (message is Array)
        {
            var prevParam:*;
            for (var i:int = 0; i < message.length; i++)
            {
                if (i == 0)
                {
                    messageString += message[i];
                    prevParam = message[i];
                    continue;
                }
                if (prevParam && prevParam is String && (prevParam as String).length > 0)
                {
                    var lastChar:String = String(prevParam).charAt(String(prevParam).length - 1);

                    if (DELIMITERS.indexOf(lastChar) > -1)
                    {
                        messageString += message[i];
                        prevParam = message[i];
                        continue;
                    }
                }
                if (message[i] is Array)
                {
                    messageString += ", [" + message[i] + "]";
                }
                else
                {
                    messageString += ", " + message[i];
                }
                prevParam = message[i];
            }
        }
        else
        {
            messageString = message;
        }

        var logString:String = timePrefix + serviceString + messageString;
        return logString;
    }

    protected static function traceLogString(logString:String):void
    {
        trace(logString);
    }

    protected function appendToLog(logString:String):void
    {
        logStrings += logString + '\n';
    }

    public function log(message:*, service:*= null):String
    {
        var logString:String = prepareLogString(service, message);

        traceLogString(logString);

        appendToLog(logString);

        return logString;
    }

    public function getLog():String
    {
        return logStrings;
    }

    public function get size():int
    {
        return logStrings.length;
    }

    public function postTo(url:String):void
    {
        var request:URLRequest = new URLRequest(url);
        request.method = URLRequestMethod.POST;
        request.data = new URLVariables();
        request.data.log = getLog();
        sendToURL(request);
    }
}
}
