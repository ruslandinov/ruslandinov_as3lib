/**
 * Author: Ruslan Gilmutdinov
 * Date: 14.04.14
 * Time: 18:35
 */
package org.ruslandinov.logger
{
import org.ruslandinov.js.ExternalInterfaceWrapper;

public class BrowserConsoleLogger extends BasicLogger
{
    public function BrowserConsoleLogger()
    {
        super();
    }

    override public function log(message:*, service:*= null):String
    {
        var logString:String = super.log(message, service);

        logToConsole(logString);

        return logString;
    }

    private static function logToConsole(logString:String):void
    {
        ExternalInterfaceWrapper.call('console.log', logString);
    }
}
}
