package org.ruslandinov.logger
{
/**
 * ...
 * @author Ruslan Gilmutdinov
 */
public class Logger
{
    private static var logger:ILogger;

    public function Logger()
    {

    }

    public static function init(loggerInstance:ILogger):void
    {
        logger = loggerInstance;
    }

    public static function log(service:*, ...message):void
    {
        logger.log(message, service);
    }

    public static function getLog():String
    {
        return logger.getLog();
    }

    public static function postTo(url:String):void
    {
        logger.postTo(url);
    }
}
}