package org.ruslandinov.events
{
    import flash.events.Event;

    /**
     * ...
     * @author Ruslan Gilmutdinov
     */
    public class OpenURLEvent extends Event
    {
        public static const OPEN_URL:String = 'OpenURLEvent.OPEN_URL';

        private var _url:String;
        private var _window:String;

        public function OpenURLEvent(url:String, window:String = '_blank')
        {
            super(OPEN_URL, true);

            _url    = url;
            _window = window;
        }

        public override function clone():Event
        {
            return new OpenURLEvent(url, window);
        }

        public override function toString():String
        {
            return formatToString("OpenURLEvent", "url", "window");
        }

        public function get url():String
        {
            return _url;
        }

        public function get window():String
        {
            return _window;
        }

    }
}